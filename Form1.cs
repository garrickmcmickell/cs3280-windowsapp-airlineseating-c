﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using System.Reflection;
using System.Data.OleDb;

namespace Assignment_6
{
    public partial class Form1 : Form
    {
        #region Variables

        /// <summary>
        /// Creates form2
        /// </summary>
        Form2 form2 = new Form2();

        /// <summary>
        /// Used to access the database.
        /// </summary>
        clsDataAccess db = new clsDataAccess();

        /// <summary>
        /// Used to update passenger and flight information.
        /// </summary>
        DataUpdate du = new DataUpdate();

        /// <summary>
        /// Used to store and modify the buttons on the main window.
        /// </summary>
        List<Button> lstButton = new List<Button>();

        /// <summary>
        /// Used to store the current list of passengers.
        /// </summary>
        List<Passenger> lstPassenger;

        /// <summary>
        /// Used to hold information about a created passenger.
        /// </summary>
        Passenger newPassenger;

        /// <summary>
        /// Used to store the current flight number.
        /// </summary>
        string flightNum;

        /// <summary>
        /// Used to determine if a seat is being changed.
        /// </summary>
        bool changingSeat;

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the main window.
        /// </summary>
        public Form1()
        {
            try
            {
                InitializeComponent();

                panel1.Visible = false;
                panel2.Visible = false;

                //btnAddPassenger.Enabled = false;
                btnChangeSeat.Enabled = false;
                btnDeletePassenger.Enabled = false;
                cboChoosePassenger.Enabled = false;
                btnAddPassenger.Enabled = false;

                du.mainForm = this;
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Opens the add passenger window.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddPassenger_Click(object sender, EventArgs e)
        {
            try
            {
                this.Hide();

                foreach (Passenger p in lstPassenger)
                {
                    foreach (Button b in lstButton)
                    {
                        if (p.seatNum == b.Text)
                        {
                            b.BackColor = Color.Red;
                        }
                    }
                }

                form2.ShowDialog();
                this.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// I didn't write this. It's getting the flight number and maybe doing something else
        /// but I don't really know. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string sSQLFlightNumber1;	
                string sSQLAircraft1;
                string sSQLFlightNumber2;
                string sSQLAircraft2; 
                string sFlight_Number1;	
                string sAircraft_Type1;
                string sFlight_Number2;
                string sAircraft_Type2;

                

                //Setup the SQL statement
                sSQLFlightNumber1 = "SELECT Flight_Number FROM FLIGHT WHERE Flight_ID = 1";
                sSQLAircraft1 = "SELECT Aircraft_Type FROM FLIGHT WHERE Flight_ID = 1";
                sSQLFlightNumber2 = "SELECT Flight_Number FROM FLIGHT WHERE Flight_ID = 2";
                sSQLAircraft2 = "SELECT Aircraft_Type FROM FLIGHT WHERE Flight_ID = 2";
                
                //Retrive the first name
                sFlight_Number1 = db.ExecuteScalarSQL(sSQLFlightNumber1);
                sAircraft_Type1 = db.ExecuteScalarSQL(sSQLAircraft1);
                sSQLFlightNumber2 = db.ExecuteScalarSQL(sSQLFlightNumber2);
                sAircraft_Type2 = db.ExecuteScalarSQL(sSQLAircraft2);

                //Put the first name in the textbox
                cboChooseFlight.Items.Add(sFlight_Number1 + " " + sAircraft_Type1);
                cboChooseFlight.Items.Add(sSQLFlightNumber2 + " " + sAircraft_Type2);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }

        }

        /// <summary>
        /// I didn't write this either. I added some stuff to it. Determines what to do
        /// when a flight is selected. Will populate the window with information depending 
        /// on the selected flight. 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboChooseFlight_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnChangeSeat.Enabled = false;

                if (cboChooseFlight.SelectedItem.ToString() == "102 Airbus A380")
                {
                    panel1.Visible = true;
                    panel2.Visible = false;
                    cboChoosePassenger.Enabled = true;
                    btnAddPassenger.Enabled = true;
                    addButtonsToList(1);
                    updatePassengers(1);
                    flightNum = "1";
                }
                else
                {
                    panel2.Visible = true;
                    panel1.Visible = false;
                    cboChoosePassenger.Enabled = true;
                    btnAddPassenger.Enabled = true;
                    addButtonsToList(2);
                    updatePassengers(2);
                    flightNum = "2";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Gets all of the passengers for a selected flight and changes the seat colors
        /// accordingly. 
        /// </summary>
        /// <param name="i"></param>
        private void updatePassengers(int i)
        {
            try
            {
                cboChoosePassenger.Text = "";
                label4.Text = "";
                btnDeletePassenger.Enabled = false;
                lstPassenger = du.getPassengers(i);

                foreach (Button b in lstButton)
                {
                    b.BackColor = Color.Blue;
                }

                foreach (Passenger p in lstPassenger)
                {
                    foreach (Button b in lstButton)
                    {
                        if (p.seatNum == b.Text)
                        {
                            b.BackColor = Color.Red;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// This is a really bad method that I used to add all of the existing seats to a
        /// list so that I can use and access them more easily.
        /// </summary>
        /// <param name="i"></param>
        private void addButtonsToList(int i)
        {
            try
            {
                lstButton = new List<Button>();

                switch (i)
                {
                    case 1:
                        lstButton.Add(btnSeat1);
                        lstButton.Add(btnSeat2);
                        lstButton.Add(btnSeat3);
                        lstButton.Add(btnSeat4);
                        lstButton.Add(btnSeat5);
                        lstButton.Add(btnSeat6);
                        lstButton.Add(btnSeat7);
                        lstButton.Add(btnSeat8);
                        lstButton.Add(btnSeat9);
                        lstButton.Add(btnSeat10);
                        lstButton.Add(btnSeat11);
                        lstButton.Add(btnSeat12);

                        foreach (Button b in lstButton)
                        {
                            b.Click += new EventHandler(btClick);
                        }

                        return;
                    case 2:
                        lstButton.Add(button7);
                        lstButton.Add(button8);
                        lstButton.Add(button9);
                        lstButton.Add(button10);
                        lstButton.Add(button11);
                        lstButton.Add(button12);
                        lstButton.Add(button13);
                        lstButton.Add(button14);
                        lstButton.Add(button15);
                        lstButton.Add(button16);
                        lstButton.Add(button17);
                        lstButton.Add(button18);
                        lstButton.Add(button19);
                        lstButton.Add(button20);
                        lstButton.Add(button21);
                        lstButton.Add(button22);
                        lstButton.Add(button23);
                        lstButton.Add(button24);
                        lstButton.Add(button25);
                        lstButton.Add(button26);

                        foreach (Button b in lstButton)
                        {
                            b.Click += new EventHandler(btClick);
                        }

                        return;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the selection is changed in the combobox. Selects the passenger
        /// and enables deleting or changing seat.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cboChoosePassenger_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                btnDeletePassenger.Enabled = true;
                btnChangeSeat.Enabled = true;

                label4.Text = ((Passenger)cboChoosePassenger.SelectedItem).seatNum;

                foreach (Passenger p in lstPassenger)
                {
                    foreach (Button b in lstButton)
                    {
                        if (p.seatNum == b.Text)
                        {
                            b.BackColor = Color.Red;
                        }
                    }
                }

                lstButton[Int32.Parse(((Passenger)cboChoosePassenger.SelectedItem).seatNum) - 1].BackColor = Color.Lime;
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens if a button is clicked. There are 3 separate section to determine
        /// whe happens if you're either changing seats, creating a passenger, or just clicking
        /// stuff.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btClick(object sender, EventArgs e)
        {
            try
            {
                Button bt = (Button)sender;

                if (bt.BackColor == Color.Red && form2.passengerSaved == false && changingSeat == false)
                {
                    foreach (Passenger p in lstPassenger)
                    {
                        if (bt.Text == p.seatNum)
                        {
                            cboChoosePassenger.SelectedItem = p;
                        }
                    }
                }
                else if (bt.BackColor != Color.Red && form2.passengerSaved == true)
                {
                    newPassenger = form2.passenger;
                    newPassenger.flightID = flightNum;
                    newPassenger.seatNum = bt.Text;
                    newPassenger.passengerID = du.createPassengerID();

                    du.addPassenger(newPassenger);

                    updatePassengers(Int32.Parse(flightNum));

                    form2.passengerSaved = false;
                }
                else if (bt.BackColor == Color.Blue && changingSeat == true)
                {
                    du.changeSeat((Passenger)cboChoosePassenger.SelectedItem, bt.Text);

                    updatePassengers(Int32.Parse(flightNum));

                    foreach (Passenger p in lstPassenger)
                    {
                        if (bt.Text == p.seatNum)
                        {
                            cboChoosePassenger.SelectedItem = p;
                        }
                    }

                    changingSeat = false;
                    btnChangeSeat.Enabled = false;
                    cboChooseFlight.Enabled = true;
                    cboChoosePassenger.Enabled = true;
                    btnAddPassenger.Enabled = true;
                    btnDeletePassenger.Enabled = true;
                    btnChangeSeat.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when the delete button is clicked. Deletes the passenger.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeletePassenger_Click(object sender, EventArgs e)
        {
            try
            {
                btnChangeSeat.Enabled = false;

                if (cboChoosePassenger.SelectedItem != null)
                {
                    int iFlight;
                    iFlight = du.deletePassenger((Passenger)cboChoosePassenger.SelectedItem);
                    updatePassengers(iFlight);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when you're changing seats. Locks everything up so that you
        /// can only select a new seat.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChangeSeat_Click(object sender, EventArgs e)
        {
            try
            {
                changingSeat = true;
                cboChooseFlight.Enabled = false;
                cboChoosePassenger.Enabled = false;
                btnAddPassenger.Enabled = false;
                btnDeletePassenger.Enabled = false;
                btnChangeSeat.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion
    }
}
