﻿namespace Assignment_6
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboChoosePassenger = new System.Windows.Forms.ComboBox();
            this.cboChooseFlight = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnChangeSeat = new System.Windows.Forms.Button();
            this.btnAddPassenger = new System.Windows.Forms.Button();
            this.btnDeletePassenger = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnSeat12 = new System.Windows.Forms.Button();
            this.btnSeat11 = new System.Windows.Forms.Button();
            this.btnSeat10 = new System.Windows.Forms.Button();
            this.btnSeat9 = new System.Windows.Forms.Button();
            this.btnSeat8 = new System.Windows.Forms.Button();
            this.btnSeat7 = new System.Windows.Forms.Button();
            this.btnSeat6 = new System.Windows.Forms.Button();
            this.btnSeat5 = new System.Windows.Forms.Button();
            this.btnSeat4 = new System.Windows.Forms.Button();
            this.btnSeat3 = new System.Windows.Forms.Button();
            this.btnSeat2 = new System.Windows.Forms.Button();
            this.btnSeat1 = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.cboChoosePassenger);
            this.groupBox1.Controls.Add(this.cboChooseFlight);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(704, 28);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox1.Size = new System.Drawing.Size(621, 198);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Passenger Information";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(297, 146);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(0, 20);
            this.label4.TabIndex = 5;
            // 
            // cboChoosePassenger
            // 
            this.cboChoosePassenger.FormattingEnabled = true;
            this.cboChoosePassenger.Location = new System.Drawing.Point(302, 88);
            this.cboChoosePassenger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboChoosePassenger.Name = "cboChoosePassenger";
            this.cboChoosePassenger.Size = new System.Drawing.Size(282, 37);
            this.cboChoosePassenger.TabIndex = 4;
            this.cboChoosePassenger.SelectedIndexChanged += new System.EventHandler(this.cboChoosePassenger_SelectedIndexChanged);
            // 
            // cboChooseFlight
            // 
            this.cboChooseFlight.FormattingEnabled = true;
            this.cboChooseFlight.Location = new System.Drawing.Point(302, 42);
            this.cboChooseFlight.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboChooseFlight.Name = "cboChooseFlight";
            this.cboChooseFlight.Size = new System.Drawing.Size(282, 37);
            this.cboChooseFlight.TabIndex = 3;
            this.cboChooseFlight.SelectedIndexChanged += new System.EventHandler(this.cboChooseFlight_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(63, 135);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(226, 36);
            this.label3.TabIndex = 2;
            this.label3.Text = "Passenger\'s Seat:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(87, 42);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(200, 36);
            this.label1.TabIndex = 0;
            this.label1.Text = "Choose Flight:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(44, 88);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(248, 36);
            this.label2.TabIndex = 1;
            this.label2.Text = "Choose Passenger:";
            // 
            // btnChangeSeat
            // 
            this.btnChangeSeat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChangeSeat.Location = new System.Drawing.Point(704, 245);
            this.btnChangeSeat.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnChangeSeat.Name = "btnChangeSeat";
            this.btnChangeSeat.Size = new System.Drawing.Size(158, 38);
            this.btnChangeSeat.TabIndex = 1;
            this.btnChangeSeat.Text = "Change Seat";
            this.btnChangeSeat.UseVisualStyleBackColor = true;
            this.btnChangeSeat.Click += new System.EventHandler(this.btnChangeSeat_Click);
            // 
            // btnAddPassenger
            // 
            this.btnAddPassenger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAddPassenger.Location = new System.Drawing.Point(940, 245);
            this.btnAddPassenger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnAddPassenger.Name = "btnAddPassenger";
            this.btnAddPassenger.Size = new System.Drawing.Size(158, 38);
            this.btnAddPassenger.TabIndex = 2;
            this.btnAddPassenger.Text = "Add Passenger";
            this.btnAddPassenger.UseVisualStyleBackColor = true;
            this.btnAddPassenger.Click += new System.EventHandler(this.btnAddPassenger_Click);
            // 
            // btnDeletePassenger
            // 
            this.btnDeletePassenger.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDeletePassenger.Location = new System.Drawing.Point(1167, 245);
            this.btnDeletePassenger.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnDeletePassenger.Name = "btnDeletePassenger";
            this.btnDeletePassenger.Size = new System.Drawing.Size(158, 38);
            this.btnDeletePassenger.TabIndex = 3;
            this.btnDeletePassenger.Text = "Delete Passenger";
            this.btnDeletePassenger.UseVisualStyleBackColor = true;
            this.btnDeletePassenger.Click += new System.EventHandler(this.btnDeletePassenger_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button6);
            this.groupBox2.Controls.Add(this.button5);
            this.groupBox2.Controls.Add(this.button4);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Location = new System.Drawing.Point(890, 352);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.groupBox2.Size = new System.Drawing.Size(435, 255);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Color Key";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Lime;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Location = new System.Drawing.Point(20, 178);
            this.button6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(60, 62);
            this.button6.TabIndex = 5;
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Blue;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Location = new System.Drawing.Point(20, 108);
            this.button5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(60, 62);
            this.button5.TabIndex = 4;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.Red;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Location = new System.Drawing.Point(20, 40);
            this.button4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(60, 62);
            this.button4.TabIndex = 3;
            this.button4.UseVisualStyleBackColor = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(88, 189);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(326, 36);
            this.label7.TabIndex = 2;
            this.label7.Text = "Selected Passenger\'s seat";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(88, 118);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(183, 36);
            this.label6.TabIndex = 1;
            this.label6.Text = "Seat is empty";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(88, 51);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(174, 36);
            this.label5.TabIndex = 0;
            this.label5.Text = "Seat is taken";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnSeat12);
            this.panel1.Controls.Add(this.btnSeat11);
            this.panel1.Controls.Add(this.btnSeat10);
            this.panel1.Controls.Add(this.btnSeat9);
            this.panel1.Controls.Add(this.btnSeat8);
            this.panel1.Controls.Add(this.btnSeat7);
            this.panel1.Controls.Add(this.btnSeat6);
            this.panel1.Controls.Add(this.btnSeat5);
            this.panel1.Controls.Add(this.btnSeat4);
            this.panel1.Controls.Add(this.btnSeat3);
            this.panel1.Controls.Add(this.btnSeat2);
            this.panel1.Controls.Add(this.btnSeat1);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Location = new System.Drawing.Point(48, 18);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(411, 308);
            this.panel1.TabIndex = 5;
            // 
            // btnSeat12
            // 
            this.btnSeat12.BackColor = System.Drawing.Color.Blue;
            this.btnSeat12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat12.ForeColor = System.Drawing.Color.White;
            this.btnSeat12.Location = new System.Drawing.Point(310, 217);
            this.btnSeat12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat12.Name = "btnSeat12";
            this.btnSeat12.Size = new System.Drawing.Size(68, 54);
            this.btnSeat12.TabIndex = 17;
            this.btnSeat12.Text = "12";
            this.btnSeat12.UseVisualStyleBackColor = false;
            // 
            // btnSeat11
            // 
            this.btnSeat11.BackColor = System.Drawing.Color.Blue;
            this.btnSeat11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat11.ForeColor = System.Drawing.Color.White;
            this.btnSeat11.Location = new System.Drawing.Point(234, 217);
            this.btnSeat11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat11.Name = "btnSeat11";
            this.btnSeat11.Size = new System.Drawing.Size(68, 54);
            this.btnSeat11.TabIndex = 16;
            this.btnSeat11.Text = "11";
            this.btnSeat11.UseVisualStyleBackColor = false;
            // 
            // btnSeat10
            // 
            this.btnSeat10.BackColor = System.Drawing.Color.Blue;
            this.btnSeat10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat10.ForeColor = System.Drawing.Color.White;
            this.btnSeat10.Location = new System.Drawing.Point(106, 217);
            this.btnSeat10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat10.Name = "btnSeat10";
            this.btnSeat10.Size = new System.Drawing.Size(68, 54);
            this.btnSeat10.TabIndex = 15;
            this.btnSeat10.Text = "10";
            this.btnSeat10.UseVisualStyleBackColor = false;
            // 
            // btnSeat9
            // 
            this.btnSeat9.BackColor = System.Drawing.Color.Blue;
            this.btnSeat9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat9.ForeColor = System.Drawing.Color.White;
            this.btnSeat9.Location = new System.Drawing.Point(30, 217);
            this.btnSeat9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat9.Name = "btnSeat9";
            this.btnSeat9.Size = new System.Drawing.Size(68, 54);
            this.btnSeat9.TabIndex = 14;
            this.btnSeat9.Text = "9";
            this.btnSeat9.UseVisualStyleBackColor = false;
            // 
            // btnSeat8
            // 
            this.btnSeat8.BackColor = System.Drawing.Color.Blue;
            this.btnSeat8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat8.ForeColor = System.Drawing.Color.White;
            this.btnSeat8.Location = new System.Drawing.Point(310, 154);
            this.btnSeat8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat8.Name = "btnSeat8";
            this.btnSeat8.Size = new System.Drawing.Size(68, 54);
            this.btnSeat8.TabIndex = 13;
            this.btnSeat8.Text = "8";
            this.btnSeat8.UseVisualStyleBackColor = false;
            // 
            // btnSeat7
            // 
            this.btnSeat7.BackColor = System.Drawing.Color.Blue;
            this.btnSeat7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat7.ForeColor = System.Drawing.Color.White;
            this.btnSeat7.Location = new System.Drawing.Point(234, 154);
            this.btnSeat7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat7.Name = "btnSeat7";
            this.btnSeat7.Size = new System.Drawing.Size(68, 54);
            this.btnSeat7.TabIndex = 12;
            this.btnSeat7.Text = "7";
            this.btnSeat7.UseVisualStyleBackColor = false;
            // 
            // btnSeat6
            // 
            this.btnSeat6.BackColor = System.Drawing.Color.Blue;
            this.btnSeat6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat6.ForeColor = System.Drawing.Color.White;
            this.btnSeat6.Location = new System.Drawing.Point(106, 154);
            this.btnSeat6.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat6.Name = "btnSeat6";
            this.btnSeat6.Size = new System.Drawing.Size(68, 54);
            this.btnSeat6.TabIndex = 11;
            this.btnSeat6.Text = "6";
            this.btnSeat6.UseVisualStyleBackColor = false;
            // 
            // btnSeat5
            // 
            this.btnSeat5.BackColor = System.Drawing.Color.Blue;
            this.btnSeat5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat5.ForeColor = System.Drawing.Color.White;
            this.btnSeat5.Location = new System.Drawing.Point(30, 154);
            this.btnSeat5.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat5.Name = "btnSeat5";
            this.btnSeat5.Size = new System.Drawing.Size(68, 54);
            this.btnSeat5.TabIndex = 10;
            this.btnSeat5.Text = "5";
            this.btnSeat5.UseVisualStyleBackColor = false;
            // 
            // btnSeat4
            // 
            this.btnSeat4.BackColor = System.Drawing.Color.Blue;
            this.btnSeat4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat4.ForeColor = System.Drawing.Color.White;
            this.btnSeat4.Location = new System.Drawing.Point(310, 69);
            this.btnSeat4.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat4.Name = "btnSeat4";
            this.btnSeat4.Size = new System.Drawing.Size(68, 54);
            this.btnSeat4.TabIndex = 9;
            this.btnSeat4.Text = "4";
            this.btnSeat4.UseVisualStyleBackColor = false;
            // 
            // btnSeat3
            // 
            this.btnSeat3.BackColor = System.Drawing.Color.Blue;
            this.btnSeat3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat3.ForeColor = System.Drawing.Color.White;
            this.btnSeat3.Location = new System.Drawing.Point(234, 69);
            this.btnSeat3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat3.Name = "btnSeat3";
            this.btnSeat3.Size = new System.Drawing.Size(68, 54);
            this.btnSeat3.TabIndex = 8;
            this.btnSeat3.Text = "3";
            this.btnSeat3.UseVisualStyleBackColor = false;
            // 
            // btnSeat2
            // 
            this.btnSeat2.BackColor = System.Drawing.Color.Blue;
            this.btnSeat2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat2.ForeColor = System.Drawing.Color.White;
            this.btnSeat2.Location = new System.Drawing.Point(106, 69);
            this.btnSeat2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat2.Name = "btnSeat2";
            this.btnSeat2.Size = new System.Drawing.Size(68, 54);
            this.btnSeat2.TabIndex = 7;
            this.btnSeat2.Text = "2";
            this.btnSeat2.UseVisualStyleBackColor = false;
            // 
            // btnSeat1
            // 
            this.btnSeat1.BackColor = System.Drawing.Color.Blue;
            this.btnSeat1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSeat1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSeat1.ForeColor = System.Drawing.Color.White;
            this.btnSeat1.Location = new System.Drawing.Point(30, 69);
            this.btnSeat1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSeat1.Name = "btnSeat1";
            this.btnSeat1.Size = new System.Drawing.Size(68, 54);
            this.btnSeat1.TabIndex = 6;
            this.btnSeat1.Text = "1";
            this.btnSeat1.UseVisualStyleBackColor = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(168, 31);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(78, 36);
            this.label8.TabIndex = 0;
            this.label8.Text = "A380";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button26);
            this.panel2.Controls.Add(this.button25);
            this.panel2.Controls.Add(this.button24);
            this.panel2.Controls.Add(this.button23);
            this.panel2.Controls.Add(this.button22);
            this.panel2.Controls.Add(this.button21);
            this.panel2.Controls.Add(this.button20);
            this.panel2.Controls.Add(this.button19);
            this.panel2.Controls.Add(this.button18);
            this.panel2.Controls.Add(this.button17);
            this.panel2.Controls.Add(this.button16);
            this.panel2.Controls.Add(this.button15);
            this.panel2.Controls.Add(this.button14);
            this.panel2.Controls.Add(this.button13);
            this.panel2.Controls.Add(this.button12);
            this.panel2.Controls.Add(this.button11);
            this.panel2.Controls.Add(this.button10);
            this.panel2.Controls.Add(this.button9);
            this.panel2.Controls.Add(this.button8);
            this.panel2.Controls.Add(this.button7);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Location = new System.Drawing.Point(48, 18);
            this.panel2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(411, 523);
            this.panel2.TabIndex = 6;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.Blue;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button26.ForeColor = System.Drawing.Color.White;
            this.button26.Location = new System.Drawing.Point(310, 357);
            this.button26.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(68, 54);
            this.button26.TabIndex = 37;
            this.button26.Text = "20";
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.Blue;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Location = new System.Drawing.Point(234, 357);
            this.button25.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(68, 54);
            this.button25.TabIndex = 36;
            this.button25.Text = "19";
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.Blue;
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button24.ForeColor = System.Drawing.Color.White;
            this.button24.Location = new System.Drawing.Point(106, 357);
            this.button24.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(68, 54);
            this.button24.TabIndex = 35;
            this.button24.Text = "18";
            this.button24.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.Blue;
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button23.ForeColor = System.Drawing.Color.White;
            this.button23.Location = new System.Drawing.Point(30, 357);
            this.button23.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(68, 54);
            this.button23.TabIndex = 34;
            this.button23.Text = "17";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.Blue;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Location = new System.Drawing.Point(310, 294);
            this.button22.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(68, 54);
            this.button22.TabIndex = 33;
            this.button22.Text = "16";
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.Blue;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button21.ForeColor = System.Drawing.Color.White;
            this.button21.Location = new System.Drawing.Point(234, 294);
            this.button21.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(68, 54);
            this.button21.TabIndex = 32;
            this.button21.Text = "15";
            this.button21.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.Blue;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Location = new System.Drawing.Point(106, 294);
            this.button20.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(68, 54);
            this.button20.TabIndex = 31;
            this.button20.Text = "14";
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button19
            // 
            this.button19.BackColor = System.Drawing.Color.Blue;
            this.button19.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button19.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button19.ForeColor = System.Drawing.Color.White;
            this.button19.Location = new System.Drawing.Point(30, 294);
            this.button19.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(68, 54);
            this.button19.TabIndex = 30;
            this.button19.Text = "13";
            this.button19.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.Blue;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.ForeColor = System.Drawing.Color.White;
            this.button18.Location = new System.Drawing.Point(310, 192);
            this.button18.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(68, 54);
            this.button18.TabIndex = 29;
            this.button18.Text = "12";
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.Blue;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Location = new System.Drawing.Point(234, 192);
            this.button17.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(68, 54);
            this.button17.TabIndex = 28;
            this.button17.Text = "11";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Blue;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Location = new System.Drawing.Point(106, 192);
            this.button16.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(68, 54);
            this.button16.TabIndex = 27;
            this.button16.Text = "10";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.Blue;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Location = new System.Drawing.Point(30, 191);
            this.button15.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(68, 54);
            this.button15.TabIndex = 26;
            this.button15.Text = "9";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.Blue;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(310, 129);
            this.button14.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(68, 54);
            this.button14.TabIndex = 25;
            this.button14.Text = "8";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.Blue;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(234, 128);
            this.button13.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(68, 54);
            this.button13.TabIndex = 24;
            this.button13.Text = "7";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Blue;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.ForeColor = System.Drawing.Color.White;
            this.button12.Location = new System.Drawing.Point(106, 129);
            this.button12.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(68, 54);
            this.button12.TabIndex = 23;
            this.button12.Text = "6";
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.Blue;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(30, 128);
            this.button11.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(68, 54);
            this.button11.TabIndex = 22;
            this.button11.Text = "5";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Blue;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(310, 66);
            this.button10.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(68, 54);
            this.button10.TabIndex = 21;
            this.button10.Text = "4";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.Blue;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(234, 66);
            this.button9.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(68, 54);
            this.button9.TabIndex = 20;
            this.button9.Text = "3";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.Blue;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Location = new System.Drawing.Point(106, 66);
            this.button8.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(68, 54);
            this.button8.TabIndex = 19;
            this.button8.Text = "2";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Blue;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(30, 65);
            this.button7.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(68, 54);
            this.button7.TabIndex = 7;
            this.button7.Text = "1";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(168, 26);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(60, 36);
            this.label9.TabIndex = 18;
            this.label9.Text = "767";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1352, 660);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.btnDeletePassenger);
            this.Controls.Add(this.btnAddPassenger);
            this.Controls.Add(this.btnChangeSeat);
            this.Controls.Add(this.groupBox1);
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cboChooseFlight;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnChangeSeat;
        private System.Windows.Forms.Button btnAddPassenger;
        private System.Windows.Forms.Button btnDeletePassenger;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSeat12;
        private System.Windows.Forms.Button btnSeat11;
        private System.Windows.Forms.Button btnSeat10;
        private System.Windows.Forms.Button btnSeat9;
        private System.Windows.Forms.Button btnSeat8;
        private System.Windows.Forms.Button btnSeat7;
        private System.Windows.Forms.Button btnSeat6;
        private System.Windows.Forms.Button btnSeat5;
        private System.Windows.Forms.Button btnSeat4;
        private System.Windows.Forms.Button btnSeat3;
        private System.Windows.Forms.Button btnSeat2;
        private System.Windows.Forms.Button btnSeat1;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.ComboBox cboChoosePassenger;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Button button26;
        public System.Windows.Forms.Button button25;
        public System.Windows.Forms.Button button24;
        public System.Windows.Forms.Button button23;
        public System.Windows.Forms.Button button22;
        public System.Windows.Forms.Button button21;
        public System.Windows.Forms.Button button20;
        public System.Windows.Forms.Button button19;
        public System.Windows.Forms.Button button18;
        public System.Windows.Forms.Button button17;
        public System.Windows.Forms.Button button16;
        public System.Windows.Forms.Button button15;
        public System.Windows.Forms.Button button14;
        public System.Windows.Forms.Button button13;
        public System.Windows.Forms.Button button12;
        public System.Windows.Forms.Button button11;
        public System.Windows.Forms.Button button10;
        public System.Windows.Forms.Button button9;
        public System.Windows.Forms.Button button8;
        public System.Windows.Forms.Button button7;
        public System.Windows.Forms.Label label9;
    }
}

