﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment_6
{
    public class Passenger
    {
        /// <summary>
        /// Stores the flightID number.
        /// </summary>
        public string flightID { get; set; }

        /// <summary>
        /// Stores the passenger ID number.
        /// </summary>
        public string passengerID { get; set;}

        /// <summary>
        /// Stores the passesnger's first name. 
        /// </summary>
        public string firstName { get; set; }

        /// <summary>
        /// Stores the passenger's last name.
        /// </summary>
        public string lastName { get; set; }

        /// <summary>
        /// Stores the passenger's seat number.
        /// </summary>
        public string seatNum { get; set; }

        /// <summary>
        /// Overrides the class to string method. Returns the first and last name concat.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return firstName + " " + lastName;
        }

        /// <summary>
        /// Method to get the seat number.
        /// </summary>
        /// <returns></returns>
        public string getSeat()
        {
            return seatNum;
        }
    }
}
