﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.ToolBar;

namespace Assignment_6
{
    class DataUpdate
    {
        #region Variables
        /// <summary>
        /// Used to hold the data retrieved from the database.
        /// </summary>
        DataSet returnData;

        /// <summary>
        /// Used to access the database.
        /// </summary>
        clsDataAccess db = new clsDataAccess();

        /// <summary>
        /// Used to create and return a list of passengers.
        /// </summary>
        List<Passenger> lstPassengers = new List<Passenger>();

        /// <summary>
        /// Used to hold the number of entries returned from the database.
        /// </summary>
        int iRet;

        /// <summary>
        /// Used to modify elements of the main form.
        /// </summary>
        public Form1 mainForm { get; set; }

        #endregion

        #region Methods
        /// <summary>
        /// Gets the information about passengers on a flight, adds them to a list, and returns
        /// the list.
        /// </summary>
        /// <param name="iFlight"></param>
        /// <returns></returns>
        public List<Passenger> getPassengers(int iFlight)
        {
            try
            {
                string sSQL = "SELECT Passenger.Passenger_ID, Passenger.First_Name, Passenger.Last_Name, Flight_Passenger_Link.Flight_ID, Flight_Passenger_Link.Seat_Number " +
                                                "FROM Passenger INNER JOIN Flight_Passenger_Link ON Passenger.[Passenger_ID] = Flight_Passenger_Link.[Passenger_ID] " +
                                                "WHERE(([Flight_Passenger_Link].[Flight_ID] = " + iFlight.ToString() + ")) ";
                iRet = 0;
                returnData = new DataSet();
                Passenger passenger;

                returnData = db.ExecuteSQLStatement(sSQL, ref iRet);

                foreach (Passenger p in lstPassengers)
                {
                    mainForm.cboChoosePassenger.Items.Remove(p);
                }

                lstPassengers = new List<Passenger>();

                for (int i = 0; i < iRet; i++)
                {
                    passenger = new Passenger();


                    passenger.passengerID = returnData.Tables[0].Rows[i][0].ToString();
                    passenger.firstName = returnData.Tables[0].Rows[i][1].ToString();
                    passenger.lastName = returnData.Tables[0].Rows[i][2].ToString();
                    passenger.flightID = returnData.Tables[0].Rows[i][3].ToString();
                    passenger.seatNum = returnData.Tables[0].Rows[i][4].ToString();

                    mainForm.cboChoosePassenger.Items.Add(passenger);
                    lstPassengers.Add(passenger);


                }
                return lstPassengers;
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
            return lstPassengers;
        }

        /// <summary>
        /// Deletes a passenger from the database.
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public int deletePassenger(Passenger p)
        {
            try
            {
                string sSQL = "Delete FROM FLIGHT_PASSENGER_LINK " +
                              "WHERE FLIGHT_ID = " + p.flightID + " AND " +
                              "PASSENGER_ID = " + p.passengerID;

                db.ExecuteNonQuery(sSQL);

                //Delete the passenger
                sSQL = "Delete FROM PASSENGER " +
                       "WHERE PASSENGER_ID = " + p.passengerID;

                db.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }

            return Int32.Parse(p.flightID); 
                         
        }

        /// <summary>
        /// Adds a passenger into the database.
        /// </summary>
        /// <param name="passenger"></param>
        public void addPassenger(Passenger passenger)
        {
            try
            {
                string sSQL = "INSERT INTO PASSENGER(Passenger_ID, First_Name, Last_Name) VALUES( " + passenger.passengerID + ",'" + passenger.firstName + "','" + passenger.lastName + "')";

                db.ExecuteNonQuery(sSQL);

                sSQL = "INSERT INTO Flight_Passenger_Link(Flight_ID, Passenger_ID, Seat_Number) " +
                       "VALUES( " + passenger.flightID + " , " + passenger.passengerID + " , " + passenger.seatNum + ")";

                db.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Generates a passengerID
        /// </summary>
        /// <returns></returns>
        public string createPassengerID()
        {
            try
            {
                iRet = 0;
                returnData = new DataSet();

                string sSQL = "SELECT PASSENGER_ID " +
                              "FROM FLIGHT_PASSENGER_LINK";

                returnData = db.ExecuteSQLStatement(sSQL, ref iRet);

                if (iRet != 0)
                {
                    int[] passengerIDs = new int[iRet];

                    for (int i = 0; i < iRet; i++)
                    {
                        passengerIDs[i] = Int32.Parse(returnData.Tables[0].Rows[i][0].ToString());
                    }

                    Array.Sort(passengerIDs);

                    return (passengerIDs.Last() + 1).ToString();
                }
                else
                {
                    return "1";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
            return "1";
        }

        /// <summary>
        /// Changes the passengers seat number in the database.
        /// </summary>
        /// <param name="passenger"></param>
        /// <param name="seatNum"></param>
        public void changeSeat(Passenger passenger, string seatNum)
        {
            try
            {
                string sSQL = "UPDATE FLIGHT_PASSENGER_LINK " +
                              "SET Seat_Number = '" + seatNum + "' " +
                              "WHERE FLIGHT_ID = " + passenger.flightID + " AND PASSENGER_ID = " + passenger.passengerID;

                db.ExecuteNonQuery(sSQL);
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }
        #endregion
    }
}
