﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Assignment_6
{
    public partial class Form2 : Form
    {
        #region Variables

        /// <summary>
        /// Used to save the entered informtion for the passenger.
        /// </summary>
        public Passenger passenger = new Passenger();

        /// <summary>
        /// Used to determine if information was saved.
        /// </summary>
        public bool passengerSaved { get; set; }

        #endregion

        #region Methods

        /// <summary>
        /// Initializes the form.
        /// </summary>
        public Form2()
        {
            try
            {
                InitializeComponent();
                passengerSaved = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Handles what happens when window is closed. Resets the fields on the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                textBox1.Text = "";
                textBox2.Text = "";

                this.Hide();
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        /// <summary>
        /// Method to handle what happens when the save button is clicked. Saves the entered
        /// information and then resets the fields.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (textBox1.Text != null && textBox2.Text != null)
                {
                    passenger.firstName = textBox1.Text;
                    passenger.lastName = textBox2.Text;
                    passengerSaved = true;

                    textBox1.Text = "";
                    textBox2.Text = "";

                    this.Hide();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(MethodInfo.GetCurrentMethod().DeclaringType.Name + "." + MethodInfo.GetCurrentMethod().Name + " -> " + ex.Message);
            }
        }

        #endregion
    }
}
